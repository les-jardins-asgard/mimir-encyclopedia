﻿// <copyright file="ApplicationDbContext.cs" company="SUID">
// This website is protected by french author's rights. Download, Modification, Use are forbidden without any authorization from the owner.
// </copyright>

namespace MimirEncyclopedia.Data
{
	using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
	using Microsoft.EntityFrameworkCore;

	/// <summary>
	/// Db context of the application.
	/// </summary>
	public class ApplicationDbContext : IdentityDbContext
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationDbContext"/> class.
		/// </summary>
		/// <param name="options">The injected options.</param>
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
		}
	}
}
